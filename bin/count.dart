import 'dart:io';

import 'package:count/count.dart' as count;

void main() {
  print('Please input your sentence: ');
  String str = stdin.readLineSync()!.toLowerCase();
  List lis = str.split(" ");
  var countList = lis.toSet().toList();
  var strList = countList.join(' ');
  var count = countList.length;
  print('---------------------------------------------------------');
  print('!!!The sentence without duplicate words is: "$strList"');
  print('---------------------------------------------------------');
  print('!!!Total of world in this sentence is: "$count"');
  print('---------------------------------------------------------');

  var countWord = Map();
  for (var element in lis) {
    if (!countWord.containsKey(element)) {
      countWord[element] = 1;
    } else {
      countWord[element] += 1;
    }
  }

  print('!!!Total of each word in this sentence is: "$countWord"');
}
